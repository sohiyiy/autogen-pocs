import autogen
import os
import openai
openai.api_type = "azure"
openai.api_base = "https://zycus-openai-pdt-poc.openai.azure.com/"
openai.api_version = "2023-09-15-preview"
openai.api_key = ''
# response = openai.Completion.create(
#     engine="gpt-35-turbo-0301",
#     prompt="",
#     temperature=1,
#     max_tokens=100,
#     top_p=0.5,
#     frequency_penalty=0,
#     presence_penalty=0,
#     stop=None)





config_list = [
    {
        'model': 'gpt-3.5-turbo',
        'api_key': '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'
    }
]

llm_config={
    # "request_timeout": 600,
    "seed": 42,
    "config_list": config_list,
    "temperature": 0
}

assistant = autogen.AssistantAgent(
    name="CTO",
    llm_config=llm_config,
    system_message="Chief technical officer of a tech company"
)

user_proxy = autogen.UserProxyAgent(
    name="user_proxy",
    human_input_mode="NEVER",
    max_consecutive_auto_reply=10,
    is_termination_msg=lambda x: x.get("content", "").rstrip().endswith("TERMINATE"),
    code_execution_config={"work_dir": "web"},
    llm_config=llm_config,
    system_message="""Reply TERMINATE if the task has been solved at full satisfaction.
Otherwise, reply CONTINUE, or the reason why the task is not solved yet."""
)

task = """
Write python code to output numbers 1 to 100, and then store the code in a file
"""

user_proxy.initiate_chat(
    assistant,
    message=task
)

task2 = """
Change the code in the file you just created to instead output numbers 1 to 200
"""

user_proxy.initiate_chat(
    assistant,
    message=task2
)